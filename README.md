# vuejs_time_picker
基于vuejs开发的移动端日历组件,目前基于vuejs的移动端日历选择器并不是很理想，于是自己写一个轻量级的日历选择器，代码都尽可能写了注释，有独特需求的可以自己添加暴露借口
# 实例
该组件已在我的另一个基于vuejs开发的微信前端商城使用，目前正在开发中
[EasyMALL](https://gitee.com/Daniel_Deng_Haibiao/EasyShop/blob/master/src/view/center/profile.vue)

# 使用说明
> 将根目录的timepicker.vue文件基于vuejs的webpack工程中以组件形式引入即可
# 组件对外暴露的借口
* handlerSelect
> 用户点击日期触发，事件回调的参数为当前的日期对象
* handlerComfirm
> 用户确定日期触发，事件回调的参数为当前的日期对象
* handlerCancel
> 用户取消触发

# 代码例子 html
```javascript
        <m-cell title="生日" :value="info.birthday | timeStampFormat"
        @click.native="interact.timePickerShow = !interact.timePickerShow"></m-cell>


        <m-timepicker
        @update:show="val => interact.timePickerShow = val" :show="interact.timePickerShow"
        @handlerComfirm="selectDay"/>

```
# 代码例子 js
```javascript
<script>
    import MTimepicker from '../componets/MTimepicker'
    export default {
        data() {
            return {
                interact: {
                    timePickerShow: false
                },
                info: {
                    birthday: 775197285000
                }
            }
        },
        components:{
            MTimepicker
        },
        methods: {
            selectDay(e) {
                this.info.birthday = e.getTime();
            },
        }
    }
</script>

```